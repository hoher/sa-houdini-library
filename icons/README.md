The icons in this directory are used by shelf tools. When installing this
library with Houdini-manage, they can reference the icons using

    $HLIBPATH_SA_HOUDINI_LIBRARY/icons/<icon_name>.png