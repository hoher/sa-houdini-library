#Samuel Alonso Houdini Library

## Installation

Use the Download button on the upper left and choose "Download ZIP".
Alternatively, if you're familiar with Git, you can also clone this repository
and pull to get updates.

Once you've placed the library in a directory of your choice, update your
Houdini environment like so (by editing the `houdini.env` file in your user
preferences folder):

```
# Make sure HOUDINI_PATH has its default value. Omit this line if there is
# already one like this, otherwise make sure that it's the first line in
# the file.
HOUDINI_PATH="&"

# Add the library to the HOUDINI_PATH. Replace the path with the absolute
# path to where you placed the library. Use : instead of ; on macOS.
HOUDINI_PATH="$HOUDINI_PATH;C:\Users\samuel\repos\houdini-library"
```
